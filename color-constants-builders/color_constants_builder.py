import os
import time
import re
import requests
from bs4 import BeautifulSoup


CURRENT_DIR = os.getcwd()
DATA_DIR = f"{CURRENT_DIR}/data"
DOWNLOAD_DIR = "/tmp2/color_downloads"
COLOR_256_INDEX_RGB_HEX_FILE = f"{DATA_DIR}/color256_index_rgb_hex.tsv"


###########################################################################
# Main
###########################################################################

def main():
    menu_text = build_menu_text()
    menu_choice = ""
    while menu_choice != "Q" and "q":
        print(menu_text)
        menu_choice = input("Enter Choice: ").upper()
        if menu_choice == "Q":
            pass
        elif menu_choice == "A":
            get_256_color_indexes_rgb_hex()
        elif menu_choice == "B":
            get_256_color_colorhexa_data()
        else:
            print("*** INVALID CHOICE ***")
    print("Done.")


def build_menu_text():
    menu_dict = {"A": "Get 256-Color Indexes/RGB Values (Wikipedia ANSI Esc Codes)",
                 "B": "Get ColorHexa Data for 256-Color Palette",
                 "Q": "Quit"}
    menu_text = "\n============================\n"
    menu_text += "Color Constants Builder Menu\n"
    menu_text += "============================\n\n"
    for key, desc in menu_dict.items():
        menu_text += f"{key}) {desc}\n"
    menu_text += "\n"
    return menu_text


###########################################################################
# Get the rgb / hex values for each of the indexes in the
# 256-color palette.
###########################################################################

def get_256_color_indexes_rgb_hex():
    url = "https://en.wikipedia.org/wiki/ANSI_escape_code"
    html_file_name = f"{DOWNLOAD_DIR}/wikipedia_ansi_escape_code.html"
    data_file_name = COLOR_256_INDEX_RGB_HEX_FILE
    print("Get Index / RGB / Hex values for 256-color palette")
    download_web_page(url, html_file_name)
    get_256_color_indexes_rgb_hex_parse_html(html_file_name, data_file_name)


def get_256_color_indexes_rgb_hex_parse_html(html_file_name, data_file_name):
    color_list = []
    print("Parsing 256-Color Palette Data")
    with open(html_file_name, 'r') as f:
        soup = BeautifulSoup(f, "html5lib")
    eight_bit_header = soup.find(id="8-bit").parent
    colors_table_0_to_255 = eight_bit_header.find_next_sibling("table")
    for td in colors_table_0_to_255.find_all("td", title=True, style=True):
        get_256_color_indexes_rgb_hex_parse_table_data(td, color_list)
    print("Writing 256-Color Palette Data")
    with open(data_file_name, 'w') as f:
        for color in color_list:
            # index -> hex -> red -> green -> blue
            f.write(f"{color[0]}\t{color[1]}\t{color[2][0]}\t{color[2][1]}\t{color[2][2]}\n")


def get_256_color_indexes_rgb_hex_parse_table_data(td, color_list):
    hx = td["title"].strip().replace("#", "").upper()
    index = td.text.strip()
    rgb = hex_to_rgb(hx)
    color_list.append([index, hx, rgb])


###########################################################################
# Use ColorHexa to get info for 256-Color Palette
###########################################################################

def get_256_color_colorhexa_data():
    color_256_list = load_256_color_list_by_index(COLOR_256_INDEX_RGB_HEX_FILE)
    # Download ColorHexa pages
    # for color_data in color_256_list:
    #     hex_lower = color_data[1].lower()
    #     url = f"https://www.colorhexa.com/{hex_lower}"
    #     html_file_name = f"{DOWNLOAD_DIR}/ColorHexa/colorhexa_{hex_lower}.html"
    #     download_web_page(url, html_file_name, sleep=True)
    # Get color descriptions
    for color_data in color_256_list:
        index = color_data[0]
        if 16 <= index <= 231:
            get_256_color_colorhexa_data_parse_html(color_data)


def get_256_color_colorhexa_data_parse_html(color_data):
    hex_lower = color_data[1].lower()
    index = color_data[0]
    html_file_name = f"{DOWNLOAD_DIR}/ColorHexa/colorhexa_{hex_lower}.html"
    with open(html_file_name, 'r') as f:
        soup = BeautifulSoup(f, "html5lib")
    info_section = soup.find("section", id="information")
    # Get the generic color description
    color_desc = info_section.find("div", class_="color-description").strong.text
    # Look for alternate names
    alt_name_text = ""
    color_detail = info_section.find("p", class_="description").find("span", class_="highlight").text
    if "(also known as " in color_detail:
        color_re = r'(\(also known as )(.+)(\))'
        m = re.search(color_re, color_detail)
        alt_name_text = m.group(2)
    print(f"{index}|{hex_lower}|{color_desc}|{alt_name_text}")


###########################################################################
# Conversion Utilities
###########################################################################

def hex_to_rgb(hex_color):
    """Given a hex color, convert to a tuple of (red, green, blue)."""
    red = int(hex_color[0:2], 16)
    green = int(hex_color[2:4], 16)
    blue = int(hex_color[4:6], 16)
    return red, green, blue


def rgb_to_hex(rgb):
    """Given a tuple (red, green, blue), return the hexadecimal string."""
    return f"{rgb[0]:02X}{rgb[1]:02X}{rgb[2]:02X}"


###########################################################################
# File Utilities
###########################################################################

def download_web_page(url, file_name, sleep=False):
    """Given a url, download it and save to the given file_name."""
    print("Downloading web page to file")
    print(f"  url: {url}")
    print(f"  file name: {file_name}")
    if os.path.exists(file_name):
        print("  File already exists, skipping download")
    else:
        print("Requesting web page...")
        response = requests.get(url)
        print("  Completed")
        if response.status_code != 200:
            print(f"ERROR: Failed to download web page, status: {response.status_code}")
            quit(1)
        print(f"Writing html to file: {file_name}")
        with open(file_name, 'w') as f:
            f.write(response.text)
        print("  Completed")
        if sleep:
            print("Pausing for 5 seconds...")
            time.sleep(5)


def load_256_color_dict_by_hex(file_name):
    """Load the data from file_name using the hex value as the key."""
    # index -> hex -> red -> green -> blue
    color_dict = {}
    with open(file_name, 'r') as f:
        for line in f:
            parts = line.split('\t')
            hx = parts[1]
            index = int(parts[0])
            red = int(parts[2])
            green = int(parts[3])
            blue = int(parts[4])
            if hx not in color_dict.keys():
                color_dict[hx] = {}
                color_dict[hx]["rgb"] = (red, green, blue)
            if "index" not in color_dict[hx]:
                color_dict[hx]["index"] = []
            color_dict[hx]["index"].append(index)
    return color_dict


def load_256_color_list_by_index(file_name):
    """Load the data from file_name as a list in index order."""
    # index -> hex -> red -> green -> blue
    color_list = []
    with open(file_name, 'r') as f:
        for line in f:
            parts = line.split('\t')
            hx = parts[1]
            index = int(parts[0])
            red = int(parts[2])
            green = int(parts[3])
            blue = int(parts[4])
            color_list.append([index, hx, (red, green, blue)])
    return color_list


###########################################################################
# Start Program
###########################################################################

if __name__ == "__main__":
    main()
