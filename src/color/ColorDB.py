import sqlite3


class ColorDB:

    def __init__(self, file_name):
        self.is_open = False
        self.file_name = file_name
        self.connection = None
        self.create_tables()

    def open(self):
        """Open the database connection if it is not already open."""
        if not self.is_open:
            self.connection = sqlite3.connect(self.file_name)
            self.is_open = True

    def close(self):
        """Close the database connection if it is not already closed."""
        if self.is_open:
            self.connection.close()
            self.is_open = False

    def create_tables(self):
        self.open()
        sources_tbl_sql = ("CREATE TABLE IF NOT EXISTS SOURCES " +
                           "(ID TEXT, " +
                           "NAME TEXT, " +
                           "CATEGORY TEXT, " +
                           "DESCRIPTION TEXT, " +
                           "URL TEXT, " +
                           "LAST_UPDATED TEXT, " +
                           "PRIMARY KEY (ID)) ")
        self.connection.execute(sources_tbl_sql)
        colors_tbl_sql = ("CREATE TABLE IF NOT EXISTS COLORS " +
                          "(SOURCE_ID TEXT, " +
                          "HEX TEXT, " +
                          "RED INTEGER, " +
                          "GREEN INTEGER, " +
                          "BLUE INTEGER, " +
                          "SOURCE_NAME TEXT," +
                          "UNDERSCORE_NAME TEXT, " +
                          "MIXED_CASE_NAME TEXT, " +
                          "PRIMARY KEY (SOURCE_ID, HEX))")
        self.connection.execute(colors_tbl_sql)
        self.close()
