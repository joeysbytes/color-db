import re


class Color:

    HEX_CODE_PATTERN = re.compile(r'^[A-F0-9]{6}$')

    def __init__(self):
        self._source_id = None
        self._hex_code = None
        self._red = None
        self._green = None
        self._blue = None
        self._source_name = None
        self._underscore_name = None
        self._mixed_case_name = None

    def __str__(self):
        color_str = "SrcId: "
        color_str += "" if self._source_id is None else str(self._source_id)
        color_str += ", Hex: "
        color_str += "" if self._hex_code is None else str(self._hex_code)
        color_str += ", RGB: "
        color_str += "" if self._red is None else "(" + str(self._red) + ", " + str(self._green) + ", " + str(self._blue) + ")"
        color_str += ", SrcName: "
        color_str += "" if self._source_name is None else str(self._source_name)
        color_str += ", UndName: "
        color_str += "" if self._underscore_name is None else str(self._underscore_name)
        color_str += ", MixName: "
        color_str += "" if self._mixed_case_name is None else str(self._mixed_case_name)
        return color_str

    #######################################################################
    # Properties
    #######################################################################

    @property
    def source_id(self):
        return self._source_name

    @source_id.setter
    def source_id(self, new_source_id):
        self._source_id = None if new_source_id is None else str(new_source_id)

    @property
    def hex_code(self):
        return self._hex_code

    @hex_code.setter
    def hex_code(self, new_hex_code):
        if new_hex_code is None:
            self._hex_code, self._red, self._green, self._blue = None, None, None, None
        else:
            hx_code = str(new_hex_code).upper()
            if Color.HEX_CODE_PATTERN.match(hx_code):
                self._hex_code = hx_code
            else:
                raise ValueError(f"Invalid Hex Code: {new_hex_code}")
            self._hex_to_rgb()

    @property
    def rgb(self):
        return self._red, self._green, self._blue

    @rgb.setter
    def rgb(self, colors):
        if colors is None:
            self._hex_code, self._red, self._green, self._blue = None, None, None, None
        else:
            if len(colors) != 3:
                raise ValueError(f"Invalid RGB Colors: {colors}")
            red, green, blue = int(colors[0]), int(colors[1]), int(colors[2])
            if 0 <= red <= 255:
                self._red = red
            else:
                raise ValueError(f"Invalid Red Value: {red}")
            if 0 <= green <= 255:
                self._green = green
            else:
                raise ValueError(f"Invalid Green Value: {green}")
            if 0 <= blue <= 255:
                self._blue = blue
            else:
                raise ValueError(f"Invalid Blue Value: {blue}")
            self._rgb_to_hex()

    @property
    def red(self):
        return self._red

    @property
    def green(self):
        return self._green

    @property
    def blue(self):
        return self._blue

    @property
    def source_name(self):
        return self._source_name

    @source_name.setter
    def source_name(self, new_source_name):
        if new_source_name is None:
            self._source_name, self._underscore_name, self._mixed_case_name = None, None, None
        else:
            self._source_name = str(new_source_name)
            self._name_to_underscore_name()
            self._name_to_mixed_case_name()

    @property
    def underscore_name(self):
        return self._underscore_name

    @property
    def mixed_case_name(self):
        return self._mixed_case_name

    #######################################################################
    # Utilities
    #######################################################################

    def _hex_to_rgb(self):
        """Take the current hex code value and calculate new rgb values."""
        self._red = int(self._hex_code[0:2], 16)
        self._green = int(self._hex_code[2:4], 16)
        self._blue = int(self._hex_code[4:6], 16)

    def _rgb_to_hex(self):
        """Take current rgb values and calculate a new hex_code value."""
        self._hex_code = f"{self._red:02X}{self._green:02X}{self._blue:02X}"

    def _name_to_underscore_name(self):
        """Take the source name and create an underscore variable name."""
        und_name = self._source_name
        if len(und_name) > 0:
            und_name = und_name.replace(" ", "_")
            und_name = und_name.upper()
        self._underscore_name = und_name

    def _name_to_mixed_case_name(self):
        """Take the source name and create a mixed case variable name."""
        mix_name = self._source_name
        if len(mix_name) > 0:
            mix_name = mix_name.replace(" ", "")
        self._mixed_case_name = mix_name
