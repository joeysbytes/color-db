from config.config import DB_FILE_NAME
from color.ColorDB import ColorDB
from color.Color import Color


def main():
    # color_db = ColorDB(DB_FILE_NAME)
    c = Color()
    name_list = ["Dark Blue", "Dark Blue 9", "Dark 8 Blue", "7 Dark Blue",
                 "dark red", "dark red 9", "dark 8 red", "7 dark red",
                 "DarkViolet", "DarkViolet9", "Dark8Violet", "7DarkViolet",
                 "darkPurple", "darkPurple9", "dark8Purple", "7darkPurple"
                 ]
    # TODO: Finish underscore, mixed_name conversions
    # TODO: Handle stripping off # in hex code
    for name in name_list:
        c.source_name = name
        print(f"{c.source_name} - {c.underscore_name} - {c.mixed_case_name}")


if __name__ == "__main__":
    main()
