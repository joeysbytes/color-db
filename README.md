# Color DB

Utilities to get information about colors from various sources on the web.

Main aspects of color gathered:

* Hex value
* RGB value
* "Official" name of color
* Index of color in a color palette
